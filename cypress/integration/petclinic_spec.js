describe("Petclinic", function () {

    beforeEach(function () {
        cy.visit('http://localhost:5000');
    });

    it("should assert the title", function () {
        cy.title().should('include', 'PetClinic' );
    });
});
